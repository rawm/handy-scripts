#/bin/bash

####################
# This script was made on the fly, so its down and dirty. Plans to fix it up soon 
# Maybe add some xargs and error checking to let you know which files didnt link and why
# Author: rawm
# Email: raymondmintz@mf5tech.com
#
######################

#NOTE for now you have to be in the directory you are making links for... I know cheesy but I will 
#	add arguments later.

ls | while read line; do ln -s "$(pwd)/$line" "/usr/bin/$line"; done

echo "DONE"
